import firebase from "@firebase/app";
import "@firebase/firestore";
import "@firebase/storage";

import { makeId, downloadFromURL } from "./utils";
import config from "./config";

firebase.initializeApp(config);

export async function getRandomId(collection) {
  const id = makeId(6);
  const docRef = firebase
    .firestore()
    .collection(collection)
    .doc(id);
  const doc = await docRef.get();
  if (doc.exists) return await getRandomId(collection);
  else return id;
}

export async function downloadFile(id, filename) {
  const url = await firebase
    .storage()
    .ref()
    .child(`files/${id}/${filename}`)
    .getDownloadURL();
  downloadFromURL(url, filename);
}

export async function uploadFile(id, file) {
  const docRef = firebase
    .firestore()
    .collection("files")
    .doc(id);
  const doc = await docRef.get();
  if (doc.exists) {
    const oldFilename = doc.data().name;
    await firebase
      .storage()
      .ref()
      .child(`files/${id}/${oldFilename}`)
      .delete();
  }

  await docRef.set({ name: file.name });
  await firebase
    .storage()
    .ref()
    .child(`files/${id}/${file.name}`)
    .put(file);
}

export async function deleteFile(id, filename) {
  await firebase
    .firestore()
    .collection("files")
    .doc(id)
    .delete();
  await firebase
    .storage()
    .ref()
    .child(`files/${id}/${filename}`)
    .delete();
}

export default firebase;
