import React from "react";

function Links({ text }) {
  const links = text.match(/(http[s]?|ftp):\/\/[\w.]+(:\d+)?[^\s]*/gi) || [];
  const anchors = links.map((link, index) => {
    const host = link.match(/(^(http[s]?|ftp):\/\/.*?)(\/|$)/i);
    return (
      <a
        href={link}
        target="_blank"
        rel="noopener noreferrer"
        key={index}
        style={{
          float: "left",
          clear: "left"
        }}
      >
        {host[1].toLowerCase()}
      </a>
    );
  });
  return <div>{anchors}</div>;
}

export default Links;
