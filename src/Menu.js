import React from "react";

function Menu({ type, setType, setId, idCache, setIdCache }) {
  return (
    <form
      className="form-inline m-2"
      onSubmit={e => {
        setId(idCache);
        e.preventDefault();
      }}
    >
      <label className="mr-3 col-form-label font-weight-bold">Type</label>
      <div className="mr-3 form-check form-check-inline">
        <input
          className="form-check-input"
          type="radio"
          name="typeOptions"
          id="textType"
          value="text"
          checked={type === "text"}
          onChange={e => setType(e.target.value)}
        />
        <label className="form-check-label" htmlFor="textType">
          Text
        </label>
      </div>
      <div className="mr-6 form-check form-check-inline">
        <input
          className="form-check-input"
          type="radio"
          name="typeOptions"
          id="fileType"
          value="file"
          checked={type === "file"}
          onChange={e => setType(e.target.value)}
        />
        <label className="form-check-label" htmlFor="fileType">
          File
        </label>
      </div>

      <label htmlFor="textId" className="col-form-label font-weight-bold mr-3">
        Enter ID
      </label>
      <input
        type="text"
        className="form-control mr-4"
        id="textId"
        value={idCache}
        onChange={e => setIdCache(e.target.value.trim())}
        onFocus={e => e.target.select()}
        onBlur={() => setId(idCache)}
      />
      <button type="submit" className="btn btn-primary btn-sm mt-2 mt-sm-0">
        {idCache === "" ? "Create" : "Find"}
      </button>
    </form>
  );
}

export default Menu;
