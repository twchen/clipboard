import React, { useState, useEffect } from "react";
import Text from "./Text";
import File from "./File";
import Menu from "./Menu";
import firebase, { getRandomId } from "./firebase";

const DEFAULT_TYPE = "text";
const type2collection = { text: "texts", file: "files" };
const DEFAULT_ID = "default";

function App() {
  const [type, setType] = useState(DEFAULT_TYPE);
  const [id, setId] = useState(DEFAULT_ID);
  const [idCache, setIdCache] = useState(DEFAULT_ID);
  const [docRef, setDocRef] = useState(
    firebase
      .firestore()
      .collection(type2collection[DEFAULT_TYPE])
      .doc(DEFAULT_ID)
  );

  useEffect(() => {
    async function updateDocRef() {
      const collection = type2collection[type];
      if (id === "") {
        const randomId = await getRandomId(collection);
        setIdCache(randomId);
        setId(randomId);
      } else {
        setDocRef(
          firebase
            .firestore()
            .collection(collection)
            .doc(id)
        );
      }
    }
    updateDocRef();
  }, [type, id]);

  function resetId() {
    setIdCache(DEFAULT_ID);
    setId(DEFAULT_ID);
  }

  const Component = type === "text" ? Text : File;

  return (
    <div className="container">
      <Menu type={type} setType={setType} setId={setId} idCache={idCache} setIdCache={setIdCache} />
      <Component docRef={docRef} resetId={resetId} />
    </div>
  );
}

export default App;
